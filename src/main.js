import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import readXlsxFile from 'read-excel-file'
import path from '@/assets/Liste des problèmes.xlsx'

import { createPinia } from 'pinia';

const app = createApp(App)
app.use(router)

const pinia = createPinia();
app.use(pinia);

//app.mount('#app')

loadExcel(path)
  .then((boulders) => {
    const app = createApp(App) // Crée l'application avec le composant racine

    app.config.globalProperties.$boulders = boulders // Stocke les données de manière globale

    app.use(router) // Utilise Vue Router

    app.mount('#app') // Montage de l'application
  })
  .catch((error) => {
    console.error('Impossible de charger les données initiales:', error)
  })

function loadExcel(path) {
  return new Promise((resolve, reject) => {
    fetch(path)
      .then((response) => {
        if (!response.ok) throw new Error('Échec du chargement du fichier Excel')
        return response.arrayBuffer()
      })
      .then((arrayBuffer) => {
        return readXlsxFile(new Blob([arrayBuffer]))
      })
      .then((rows) => {
        const boulders = []
        for (let i = 5; i < rows.length; i++) {
          const boulder = {
            name: rows[i][3],
            grade: rows[i][1],
            description: rows[i][7],
            fa: rows[i][8],
            image: rows[i][3] + '.jpg',
            lat: Number(rows[i][5]),
            lon: Number(rows[i][6])
          }
          boulders.push(boulder)
        }
        resolve(boulders)
      })
      .catch((error) => {
        console.error('Erreur lors du traitement du fichier Excel:', error)
        reject(error)
      })
  })
}
