import { defineStore } from 'pinia';

export const userLocationStore = defineStore('location', {
  state: () => ({
    userLocation: null,
    watcherId: null
  }),
  actions: {
    requestPermission() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          // Permission accordée et position obtenue
          this.updateUserLocation(position.coords.latitude, position.coords.longitude);
        }, (error) => {
          if (error.code === error.PERMISSION_DENIED) {
            alert('Permission de géolocalisation refusée');
          }
        });
      } else {
        alert('Geolocation is not supported by this browser.');
      }
    },
    updateUserLocation(latitude, longitude) {
      this.userLocation = { latitude, longitude };
    },
    fetchUserLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.updateUserLocation(position.coords.latitude, position.coords.longitude);
        }, (error) => {
          switch (error.code) {
            case error.PERMISSION_DENIED:
              alert("User denied the request for Geolocation.");
              break;
            case error.POSITION_UNAVAILABLE:
              alert("Location information is unavailable.");
              break;
            case error.TIMEOUT:
              alert("The request to get user location timed out.");
              break;
            default:
              alert("An unknown error occurred.");
              break;
          }
        });
      } else {
        alert('Geolocation is not supported by this browser.');
      }
    },
    startLocationWatch() {
      this.stopLocationWatch(); // Arrêter l'observation précédente si elle existe
      this.watcherId = setInterval(() => {
        console.log("update position")
        this.fetchUserLocation();
      }, 5000); // Mise à jour toutes les 5 secondes
    },
    stopLocationWatch() {
      if (this.watcherId) {
        clearInterval(this.watcherId);
        this.watcherId = null;
      }
    }
  }
});
