import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue'),
      props: (route) => ({ markerId: route.query.markerId })
    },
    {
      path: '/boulders',
      name: 'boulders',
      component: () => import('../views/BouldersView.vue')
    },
    {
      path: '/boulders/:index',
      name: 'boulder',
      component: () => import('../views/BoulderView.vue')
    }
  ]
})

export default router
